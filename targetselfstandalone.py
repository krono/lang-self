"""
    Self as a standalone-target.
"""

import sys
#import optparse

from self.interpreter.selfparser import Parser, parse_file
from self.interpreter.interpreter import Interpreter

# __________  Entry point  __________

def entry_point(argv):
    argno = len(argv)
    if argno == 3:
        Interpreter(parse_file(argv[2])).run()
    elif argno == 2:
        Interpreter(Parser(argv[1]).parse()).run()
    else:
        usage(argv[0])
    return 0

# _____ Define and setup target ___

def target(driver, args):
    driver.exe_name = 'self-%(backend)s'
    return entry_point, None

def jitpolicy(driver):
    from pypy.jit.codewriter.policy import JitPolicy
    return JitPolicy()

# Helper functions

def usage(program):
    """%s: usage: %s [-f filename] [-h] [-s snapshot] ...
Options:
  -f filename	Reads filename (Self source) immediately after startup
  -h		Prints this message
  -p		Don't do `snapshotAction postRead' after reading snapshot
  -s snapshot	Reads initial world from snapshot
  -w	Suppress warnings about optimized code being discarded
For debugging use only:
  -F		Discards saved code from snapshot
  -l logfile	write spy log to logfile
  -r		Disable real timer
  -t		Disable all timers
  -c		Use real timer instead of CPU timer (for OS X)
  -o		Oversample the timers (Run them 10x faster to flush out bugs)
  -a		Test the Assembler (added for Intel)
Other command line switches may be interpreted by the Self world
"""
    print usage.__doc__ % (program, program)

if __name__ == '__main__':
    entry_point(sys.argv)
