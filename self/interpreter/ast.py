#!/bin/python   
# coding: utf-8
from pypy.rlib.objectmodel import we_are_translated

from pypy.rlib.parsing.tree import Node as PyNode
from pypy.rlib.parsing.lexer import SourcePos

def append_argument(string, arg_string):
    """append the argument string to the string, adding ', ' if neccesary"""
    closed = False
    if string[-1] == ")": string = string[:-1]; closed = True    
    if string[-1] != "(": string += ", "
    
    string += arg_string
    
    if closed: string += ")"
    
    return string


class Node(PyNode):

    def __init__(self, source_position=None):
        self.source_position = source_position or SourcePos(-1,-1,-1)

    def __eq__(self, other):
        if not isinstance(other, Node):
            return False
        
        return type(self) is type(other) and self._dictmatch(other, ignoring=['source_position'])
    
    def _dictmatch(self, other, ignoring=[]):
        return {k : v for k, v in  self.__dict__.iteritems() if not k in ignoring } \
            == {k : v for k, v in other.__dict__.iteritems() if not k in ignoring }
    
    def __repr__(self):
        if False:
            return append_argument("%s()" % self.__class__.__name__ , "source_position=%r" % self.source_position)
        else:
            return "%s()" % self.__class__.__name__
    
    dot_shape = "ellipse"
    
    def dot(self):
        info = (str(self).replace("\\", "\\\\")
                         .replace('"', '\\"')
                         .replace('\n', '\\l'))
        yield '"%s" [shape=%s,label="%s"];' % (id(self), self.dot_shape, info)
        for child in self.node_children():
            yield '"%s" -> "%s";' % (id(self), id(child))
            if isinstance(child, Node):
                for line in child.dot():
                    yield line
            else:
                yield '"%s" [label="%s"];' % (
                    id(child),
                    repr(child).replace('"', '').replace("\\", "\\\\"))
            
    def getsourcepos(self):
        if self.source_position:
            return self.source_position
        else:
            children = self.node_children()
            if len(children):
                return children[0].getsourcepos()
    
    def node_children(self):
        return []
        
    def is_comment(self):
        return False


# for develompent time to mark not-implemented non-comments
class FooNode(Node):
    def __init__(self, children, symbol, source_position=None):
        Node.__init__(self, source_position=source_position)
        self.children=children
        self.symbol=symbol
    
    def node_children(self):
        return self.children
    
    def __str__(self):
        return "%s(%s)" % (self.symbol, ", ".join([str(c) for c in self.children]))

    def __repr__(self):
        return "*Nonterminal(%r, %r)" % (self.symbol, self.children)


class Comment(Node):
    def __init__(self, comment="", source_position=None):
        Node.__init__(self, source_position=source_position)
        self.comment = comment
    
    def is_comment(self):
        return True
    

class SelfSource(Node):
    def __init__(self, expressions=[], source_position=None):
        Node.__init__(self, source_position=source_position)
        self.expressions = expressions

    def __repr__(self):
        return append_argument(Node.__repr__(self), "expressions=%r" % self.expressions)
    
    def __str__(self):
        if len(self.expressions):
            return "%ss (%s)" % (self.__class__.__name__, len(self.expressions))
        else:
            return repr(self)
    
    def node_children(self):
        return Node.node_children(self) + self.expressions

class Expression(Node):
    """Expression of a self AST"""
    def __init__(self, source_position=None):
        Node.__init__(self, source_position=source_position)


class Constant(Expression):
    
    def __init__(self, source_position=None):
        Expression.__init__(self, source_position=source_position)
    
    dot_shape = "box"
    
    def get_string(self):
        if we_are_translated():
            raise NotImplementedError
        else:
            raise NotImplementedError(type(self).__name__)
        
    def get_float(self):
        if we_are_translated():
            raise NotImplementedError
        else:
            raise NotImplementedError(type(self).__name__)
        
    def get_integer(self):
        if we_are_translated():
            raise NotImplementedError
        else:
            raise NotImplementedError(type(self).__name__)

class StringConstant(Constant):
    
    def __init__(self, content, source_position=None):
        Constant.__init__(self, source_position=source_position)
        self.string_content = content
    
    def get_string(self):
        return self.string_content
    
    def __repr__(self):
        return append_argument(Constant.__repr__(self), "content=%r" % self.string_content)
        
    def __str__(self):
        return self.string_content

class FloatConstant(Constant):
    
    def __init__(self, content, source_position=None):
        Constant.__init__(self, source_position=source_position)
        self.float_content = content
    
    def get_float(self):
        return self.float_content
        
    def __repr__(self):
        return append_argument(Constant.__repr__(self), "content=%r" % self.float_content)
        
    def __str__(self):
        return str(self.float_content)
    

class IntegerConstant(Constant):
    
    def __init__(self, content, source_position=None):
        Constant.__init__(self, source_position=source_position)
        self.integer_content = content
    
    def get_integer(self):
        return self.integer_content
    
    def __repr__(self):
        return append_argument(Constant.__repr__(self), "content=%r" % self.integer_content)
        
    def __str__(self):
        return str(self.integer_content)




class Object(Expression):
    """
    A Self Object
    
    2.1.9 Construction of object literals 
    
    Object literals are constructed during parsing—the parser converts
    objects in textual form into real Self objects. An object literal is
    constructed as follows:
    
    • First, the slot initializers of every slot are evaluated from left to
      right. If a slot initializer contains another object literal, this
      literal is constructed before the initializer containing it is
      evaluated. If the initializer is an expression, it is evaluated in
      the context of the lobby.
    
    • Second, the object is created, and its slots are initialized with the
      results of the evaluations performed in the first step.
    
    Slot initializers are not evaluated in the lexical context, since none
    exists at parse time; they are evaluated in the context of an object
    known as the lobby. That is, the initializers are evaluated as if they
    were the code of a method in a slot of the lobby. This two-phase object
    construction process implies that slot initializers may not refer to
    any other slots within the constructed object (as with Scheme’s let*
    and letrec forms) and, more generally, that a slot initializer may not
    refer to any textually enclosing object literal.
    
    """
    def __init__(self, slots=[], code=[], annotation=None, source_position=None):
        Expression.__init__(self, source_position=source_position)
        self.slots = slots
        self.code = code
        self.annotation = annotation

    def has_annotation(self):
        return self.annotation is not None
        
    def node_children(self):
        return Expression.node_children(self) + self.slots + self.code
    
    def __repr__(self):
        r = Expression.__repr__(self)
        if len(self.slots):
            r = append_argument(r, "slots=%r" % self.slots)
        if len(self.code):
            r = append_argument(r, "code=%r" % self.code)
        if self.has_annotation():
            r = append_argument(r, "annotation=%r" % self.annotation)
        return r
    

# Special, probalby implicit Constants.

class TheImplicitReceiver(Expression):

    def __init__(self, source_position=None):
        Expression.__init__(self, source_position=source_position)
    
    def __repr__(self):
        return "<the implicit receiver>"
    
class Self(Expression):
    def __init__(self, source_position=None):
        Expression.__init__(self, source_position=source_position)
    
    def __repr__(self):
        return "<self>"

class TheUndirectedResendDelegate(Expression):
    def __init__(self, source_position=None):
        Expression.__init__(self, source_position=source_position)

    def __repr__(self):
        return "resend"

class Send(Expression):
    """Expression representing sends"""
    def __init__(self, selector, receiver=None, source_position=None):
        Expression.__init__(self, source_position=source_position)
        self.selector = selector
        self.receiver = receiver or TheImplicitReceiver()
    
    def is_implicit(self):
        return isinstance(self.receiver, TheImplicitReceiver)
    
    def __repr__(self):
        res = Expression.__repr__(self)
        res = append_argument(res, "selector=%r" % self.selector)
        if not self.is_implicit():
            res = append_argument(res, "receiver=%r" % self.receiver)
        return res
    
    def __str__(self):
        return str(self.selector)
    
    
    def node_children(self):
        return Expression.node_children(self) + [self.receiver]

class UnarySend(Send): pass

class BinarySend(Send):
    """Expression representing a binary send, eg, a + b"""
    def __init__(self, argument, selector, receiver=None, source_position=None):
        Send.__init__(self, selector=selector, receiver=receiver, source_position=source_position)
        self.argument = argument
    
    def __repr__(self):
        return append_argument(Send.__repr__(self), "argument=%r" % self.argument)
    
    def node_children(self):
        return Send.node_children(self) + [self.argument]

class KeywordSend(Send):
    """Expression representing a keyword send, eg, number bewteen: a And: b"""
    def __init__(self, selector_components, arguments, receiver=None, source_position=None):
        assert len(selector_components) == len(arguments)
        self.selector_components = selector_components
        Send.__init__(self, selector=self.full_selector(), 
                      receiver=receiver, source_position=source_position)
        self.arguments = arguments
    
    def full_selector(self):
        return "".join(self.selector_components)
    
    def __repr__(self):
        return append_argument(Send.__repr__(self), "arguments=%r" % self.arguments)
    
    
    def node_children(self):
        return Send.node_children(self) + self.arguments
        

class Resend(Expression):
    """ Directed or undirected resend """
    def __init__(self, delegate=None, source_position=None):
        Expression.__init__(self, source_position=source_position)
        self.delegate = delegate or TheUndirectedResendDelegate()
    
    def is_undirected(self):
        return isinstance(self.delegate, TheUndirectedResendDelegate)
    
    def is_directed(self):
        return not self.is_undirected()
    
    dot_shape = "box"
    
    def __repr__(self):
        r = Expression.__repr__(self)
        if self.is_directed():
            r = append_argument(r, "delegate=%r" % self.delegate)
        return r
    
    def __str__(self):
        return str(self.delegate)
    
    def node_children(self):
        return Expression.node_children(self)
    

# Slot descriptors

class Canister(Node):
    """ Temporary children holder during AST transformation"""
    def __init__(self, children, symbol, source_position=None):
        Node.__init__(self, source_position=source_position)
        self.children=children
        self.symbol=symbol

    def __repr__(self):
        r = append_argument(Node.__repr__(self), "symbol=%r" % self.symbol)
        r = append_argument(r, "children=%r" % self.children)
        return r
    
    def is_comment(self):
        if self.symbol == "comment":
            return True
        for c in self.children:
            if not c.is_comment(): 
                return False
        return True

class Slot(Node):
    """
    Slot in a Self object
    
    """
    def __init__(self, name, annotation=None, source_position=None):
        Node.__init__(self, source_position=source_position)
        self.annotation = annotation
        self.name = name
    
    def __repr__(self):
        r = append_argument(Node.__repr__(self), "name=%r" % self.name)
        if self.has_annotation():
            r = append_argument(r, "annotation=%r" % self.annotation)
        return r
    
    def has_annotation(self):
        return self.annotation is not None
    
    # for dot.
    dot_shape = "octagon"
    
class ArgumentSlot(Slot):
    """
    Argument slots for methods and blocks
    """
    
class DataSlot(Slot):
    """
    Non-argument slot.
    """
    def __init__(self, name, annotation=None, initializer=None, is_parent=False, assignable=False, arguments=[], argument_count=0, source_position=None):
        Slot.__init__(self, name=name, annotation=annotation, source_position=source_position)
        self.initializer = initializer
        self.is_parent = is_parent        
        # slots without initalizer become assignable slots by default
        self.assignable = assignable if initializer else True
        self.arguments = arguments
        self.argument_count = argument_count
        
    def __repr__(self):
        r = Slot.__repr__(self)
        if self.initializer:
            r = append_argument(r, "initializer=%r" % self.initializer)
            if self.assignable:
                r = append_argument(r, "assignable=%r" % self.assignable)
        if self.is_parent:
            r = append_argument(r, "is_parent=%r" % self.is_parent)
        if self.argument_count > 0:
            r = append_argument(r, "argument_count=%r" % self.argument_count)
            r = append_argument(r, "arguments=%r" % self.arguments)
        return r
    
    def node_children(self):
        return Slot.node_children(self) + [self.initializer]
    
    