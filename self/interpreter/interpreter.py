"""
    Self interpreter
"""

class Interpreter(object):
    """Self interpreter"""
    def __init__(self, parsed_source):
        self.parsed_source = parsed_source
    
    def run(self):
        print self.parsed_source
        # self.parsed_source.view()
        return self.parsed_source