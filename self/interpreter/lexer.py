import string

from pypy.rlib.parsing.lexer import Token, SourcePos
from pypy.rlib.rstring import StringBuilder

from self.util.stringutils import *

OPERATORS = list('!#$%&*+-,/;<=>?@\\^`|~')

ESCAPES = {
    "n": "\n", "t": "\t", "f": "\f", 
    "r": "\r", "v": "\v", "b": "\b", 
    "a": "\a", "0": "\x00"
}



class LexerError(Exception):
    def __init__(self, pos):
        self.pos = pos

class Lexer(object):
    """Self Lexer"""
    # inspired by rupypy
    def __init__(self):
        self.tokens = []
        self.current_value = []
        self.clobber()
        
    def __repr__(self):
        return "Lexer()"
    
    def add(self, ch):
        self.current_value.append(ch)
        
    def current_pos(self):
        return SourcePos(self.stream.tell(), self.line, self.column)
    
    def clobber(self):
        self.clear()
        del self.tokens[:]
        self.line = 0
        self.column = 0
        self.source_index = 0
        self.depth = 0
        self.stream = None
        
    def clear(self):
        del self.current_value[:]
        
        
    def read(self, n=1):
        """
        read from stream. Attention, defaults to 1 byte, unlike
        default read impl of Python.
        """
        c = self.stream.read(n)
        if len(c) > 0: 
            if c in ["\r","\n"]:
                self.step(newline=True, count=n)
            else:
                self.step(newline=False, count=n)
        assert isinstance(c, str)
        return c
        
    def peek(self):
        peeked = self.stream.peek()
        assert isinstance(peeked, str)
        if len(peeked) > 0:
            return peeked[0]
        else:
            return ""
        
        
    def tell(self):
        return self.stream.tell()
        
    def step(self, count=1, newline=False):
        if newline:
            self.line += count
            self.column = 0
        else:
            self.column += count
    
    def emit_value(self, token, value):
        self.clear()
        self.tokens.append(Token(token, value, self.current_pos()))
        
    def emit(self, token):
        self.emit_value(token, "".join(self.current_value))
        
        
    def error(self):
        raise LexerError(self.current_pos())
    
    def tokenize_string(self, string):
        return self.tokenize(StringReadStream(string))
    
    def tokenize(self, source):
        """ tokeinze source into tokens """
        self.clobber()      
        self.stream = source
        while True:
            c = self.read()

            if c == '':
                self.emit("EOF")
                return self.tokens
            elif c in ["\n","\r"]:
                self.newline(c)
            elif c in "\x09\x0a\x0b\x0c\x0d\x20\b":
                continue
            elif c == '"':
                self.comment(c)
            elif c == "(":
                self.depth += 1
                self.emit("LEFT_PARENTHESIS")
            elif c == ")":
                self.depth -= 1
                self.emit("RIGHT_PARENTHESIS")
            elif c == "[":
                self.depth += 1
                self.emit("LEFT_BRACKET")
            elif c == "]":
                self.depth -= 1
                self.emit("RIGHT_BRACKET")
            elif c == "{":
                self.emit("LEFT_BRACE")
            elif c == "}":
                self.emit("RIGHT_BRACE")
            elif c == ".":
                self.dot(c)
            elif c == "'":
                self.string(c)
            elif is_digigt_or_minus(c):
                self.number(c)
            elif c in OPERATORS:
                if c == "\\" and self.peek() in ["\n","\r"]:
                    # escaped newline: ignore
                    self.read()
                    continue
                self.operator(c)
            else:
                self.name(c)
                
    def comment(self, char):
        
        while not (self.peek() in ['"', '']):
            ch = self.read()
            if ch == "\\":
                self.add(self.read())
            else:
                self.add(ch)
        
        if self.read() != '"':
            self.error()
        self.emit("COMMENT")
    
    def _num_to_int_str(self, base=10, negative=False):
        try:
            num = int("".join(self.current_value), base)
            return str(- num if negative else num)
        except ValueError:
            self.error()

        
    def _num_to_float_str(self, negative=False):
        try:
            num = float("".join(self.current_value))
            return str(- num if negative else num)
        except ValueError:
            self.error()
        
        
        
    def number(self, char):
        
        is_negative = False
        is_float = False
        
        if char == "-":
            if is_digit(self.peek()):
                char = self.read()
                is_negative = True
            else:
                self.operator(char)
                return
        elif char == "0":
            nextchar = self.peek()
            if nextchar in ["x","X"] or is_digit(nextchar):
                # no support for 0x... or C-octals like 018
                self.read()
                self.error()
        
        self.add(char)
        while is_digit(self.peek()):
            self.add(self.read())
        
        if self.peek() in ["r","R"]:
            base = int("".join(self.current_value))
            if base < 2 or base > 36:
                self.read()
                self.error()
            self.add(self.read())
            self.number_with_base(base, is_negative)
            return
        
        if self.peek() == ".":
            c = self.read()
            if  is_digit(self.peek()):
                self.add(c)
                while is_digit(self.peek()):
                    self.add(self.read())
                is_float = True
            else:
                self.emit_value("INTEGER", self._num_to_int_str(negative=is_negative))
                self.dot(char)
                return
            
        if self.peek() in ["e","E"]:
            is_float = True 
            self.add(self.read())
            # explicit sign
            if self.peek() in ["+","-"]:
                self.add(self.read())
            # must follow digit
            if not is_digit(self.peek()):
                self.read()
                self.error()
            while is_digit(self.peek()):
                self.add(self.read())
        
        if is_float:
            self.emit_value("FLOAT", self._num_to_float_str(negative=is_negative))
        else:
            self.emit_value("INTEGER", self._num_to_int_str(negative=is_negative))
        
    def number_with_base(self, base, negative=False):
        digits = [chr(ord("0") + x) for x in xrange(min(10, base))]
        chars =  [chr(ord(ch) + x) for x in xrange(base - 10) for ch in ["a", "A"]]
        allowed_chars = [ char for charset in [digits, chars] for char in charset ]
        
        self.clear()
        if not is_alnum(self.peek()):
            # number with base but nothing expected after it.
            self.error()
        
        c = self.peek()
        while c != "" and c in allowed_chars:
            self.add(self.read())
            c = self.peek()
        
        if is_alnum(self.peek()):
            # digit/char too large for base
            self.read()
            self.error()
        
        self.emit_value("INTEGER", self._num_to_int_str(base=base, negative=negative))
        
    def name(self, char):
        
        cap = False
        prim = False
        arg = False
        kw = False
        token = "NAME"
        
        if char == ":":
            arg = True
            if not is_lower(self.peek()):
                self.read()
                self.error()
        elif char == "_":
            prim = True
            self.add(char)
        elif is_upper(char):
            cap = True
            self.add(char)
        else:
            self.add(char)

        while is_alnum_under(self.peek()):
            self.add(self.read())
            
        prelim_value = "".join(self.current_value)
        if prelim_value == "self":
            token = "SELF"
        elif prelim_value == "resend":
            token = "RESEND"
        
        if self.peek() == ":":
            if token in ["SELF", "RESEND"] or arg:
                self.read()
                self.error()
            self.add(self.read())
            kw = True
        
        if cap and not kw:
            # no capitalized names, sigh.
            self.error()
            
        
        if  arg:  token = "ARGUMENT"
        elif kw:  token = "KEYWORD"
        if prim:  token = "PRIMITIVE_" + token
        if  cap:  token = "CAP_" + token
        
        self.emit(token)
    
    def string(self, char):
        while True:
            char = self.read()
            if char == "'":
                self.emit("STRING")
                return
            elif char == "\\":
                self.escaped()
            elif char == '':
                # oops. unterminated string.
                self.error()
            else:
                self.add(char)
    
    def escaped(self):
        char = self.read()
        if char in ["\n", "\r"]:
            #escaped newline. ignore
            pass
        elif char == '':
            self.add("\\")
        elif char in ["?", "'", '"', "\\"]:
            self.add(char)
        elif char in "ntfrvba0\\":
            self.add(ESCAPES[char])
        elif char == 'x':
            self.numeric_escape(base=16, digits=2)
        elif char == 'o':
            self.numeric_escape(base=8, digits=3)
        elif char == 'd':
            self.numeric_escape(base=10, digits=3)
        else:
            self.error()
    
    def numeric_escape(self, base, digits):
        try:
            val = int(self.read(digits), base)
            if val > 255: self.error()
            self.add(chr(val))
        except ValueError:
            self.error()
        
    
    def dot(self, char):
        if is_upper_digit(self.peek()):
            self.read()
            self.error()
        self.add(char)
        self.emit("DOT")
        
    def operator(self, char):
        
        self.add(char)
        while self.peek() in OPERATORS:
            self.add(self.read())
            
        prelim_value = ''.join(self.current_value)
        if prelim_value == "<-":
            self.emit("ARROW")
        elif prelim_value == "=":
            self.emit("EQUALS")
        elif prelim_value == "|":
            self.emit("BAR")
        elif prelim_value == "^":
            self.emit("CARET")
        elif prelim_value == "*":
            self.emit("STAR")
        elif prelim_value == "\\" and self.peek() in ["\n", "\r"]:
            self.clear()
        else:
            self.emit("OPERATOR")
    
    def newline(self, char):
        if self.depth <= 0:
            self.depth = 0
            self.emit("NEWLINE")
    