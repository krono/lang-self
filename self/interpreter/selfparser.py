"""
    Parser for self.
"""
from __future__ import absolute_import

from pypy.rlib.parsing.parsing import Rule, PackratParser, ParseError as PyParseError
from pypy.rlib.parsing.tree import RPythonVisitor, Symbol, Nonterminal, Node
from pypy.rlib.objectmodel import we_are_translated
import py
import sys
import os

from .ast import *
from .lexer import Lexer

sys.setrecursionlimit(10000)
set = py.builtin.set

print_tokens = False

#
# Helper for source access
#
class Source(object):
    def __init__(self, string, filename="<None>"):
        self.filename = filename
        self.fullsource = string
        from self.util.stringutils import StringReadStream
        self.stream = StringReadStream(string)

#
# Parser helper contexts
#

class AnnotationContext(object):
    def __init__(self, parser, annotation):
        self.parser = parser
        self.annotation = annotation
    
    def __enter__(self):
        if self.parser.annotation_stack:
            self.parser.annotation_stack.append(
                self.parser.annotation_stack[-1] + "\n" + self.annotation)
        else:
            self.parser.annotation_stack.append(self.annotation)
    
    def __exit__(self, type, value, traceback):
        self.parser.annotation_stack.pop()

class SlotContext(object):
    def __init__(self, parser):
        self.parser = parser
    
    def __enter__(self):
        self.parser.slot_stack.append(DataSlot(name=None, 
                                         annotation=self.parser.get_annotation()))
    
    def __exit__(self, type, value, traceback):
        self.parser.slot_stack.pop()

#
# Error
#

class ParseError(Exception):
    def __init__(self, source_pos, errorinformation=None):
        self.source_pos = source_pos
        self.errorinformation = errorinformation
        self.args = (source_pos, errorinformation)
    
    def nice_error_message(self, filename="<unknown>", source=""):
        result = ["  File %s, line %s" % (filename, self.source_pos.lineno)]
        if source:
            result.append(source.split("\n")[self.source_pos.lineno])
            result.append(" " * self.source_pos.columnno + "^")
        else:
            result.append("<couldn't get source>")
        if self.errorinformation:
            result.append("ParseError: %s" % self.errorinformation)
        else:
            result.append("ParseError")
        return "\n".join(result)

#
# Parser/Transformator to Self AST
#

class Parser(RPythonVisitor):
    """Self Parser"""
    def __init__(self, source, is_file=False):
        RPythonVisitor.__init__(self)
        if is_file:
            with open(source) as f:
                self.source = Source(f.read(), source)
        else:
            self.source = Source(source)
        self.annotation_stack = []
        self.slot_stack = []
        self.reset_receiver()
        self.parser, self.lexer, self.transformer = make_self_parser()
    
    def parse(self):
        try:
            tokens = self.lexer.tokenize(self.source.stream)
            if not we_are_translated() and print_tokens:
                from pprint import pprint
                pprint(tokens)
            parsed = self.parser.parse(tokens)
        except ParseError, e:
            s = self.source.fullsource
            print e.nice_error_message(filename=self.source.filename,source=s)
            raise
        except PyParseError, e:
            s = self.source.fullsource
            print e.nice_error_message(filename=self.source.filename,source=s)
            raise        
        pre_ast = self.transformer().transform(parsed)
        actual_ast = self.dispatch(pre_ast)
        if not we_are_translated():
            try:
                if py.test.config.option.view: 
                    actual_ast.view()
            except AttributeError:
                pass
        return actual_ast
        
    # helper
    
    def handle_all(self, nodes):
        """ Dispatches on all nodes in list and process all comments"""
        processed_nodes = [self.dispatch(child) for child in nodes]
        #processed_nodes = self.flatten_canister(processed_nodes)
        processed_nodes = self.handle_comments(processed_nodes)
        processed_nodes = self.check_binary_sends(processed_nodes)
        return processed_nodes
    
    
    def handle_comments(self, nodes):
        """ Process comments in node list"""
        # for now, just ignore.        
        new_nodes = [x for x in nodes if not x.is_comment()]
        return new_nodes
    
    def flatten_canister(self, nodes):
        """ flatten out canisters """
        new_nodes = []
        for node in nodes:
            if hasattr(node, 'children'):
                node.children = self.flatten_canister(node.children)
            if isinstance(node, Canister):
                for subnode in node.children:
                    new_nodes.append(subnode)
            else:
                new_nodes.append(node)
        return new_nodes
    
    def pos(self, node):
        try:
            return node.getsourcepos()
        except IndexError, e:
            return None
    
    def reset_receiver(self):
        n = TheImplicitReceiver()
        self.receiver = n
    
    def get_annotation(self):
        if self.annotation_stack:
            return self.annotation_stack[-1]
    
    def get_slot(self):
        if self.slot_stack:
            return self.slot_stack[-1]
    
    
    # detokenization
    def visit_STRING(self, node):
        s = StringConstant(content=node.additional_info, source_position=node.getsourcepos())
        self.receiver = s
        return s
        
    def visit_INTEGER(self, node):
        n = IntegerConstant(content=int(node.additional_info), source_position=node.getsourcepos())
        self.receiver = n
        return n
        
    def visit_FLOAT(self, node):
        n = FloatConstant(content=float(node.additional_info), source_position=node.getsourcepos())
        self.receiver = n
        return n
        
    def visit_SELF(self, node):
        self_ = Self(source_position=node.getsourcepos())
        self.receiver = self_
        return self_
    
    # productions 
    
    def visit_expression(self, node):
        self.reset_receiver()
        
        # the last of the handled expressions
        exp = (self.handle_all(node.children))[-1]
        assert isinstance(exp, Node)
        return exp
                
    def visit_resend(self, node):
        if node.children[0].symbol == "NAME":
            delegate = self.dispatch(node.children[0])
            r = Resend(delegate=delegate, source_position=node.getsourcepos())
        else:
            r = Resend(source_position=node.getsourcepos())
        self.receiver = r
        return r
    
    #
    # sends
    #
    def visit_unary_send(self, node):
        u = UnarySend(
                    receiver=self.receiver,
                    selector=self.dispatch(node.children[0]).get_string(), 
             source_position=node.getsourcepos())
        self.receiver = u
        return u

    def visit_binary_send(self, node):

        # for comments.
        receiver = self.receiver
        self.reset_receiver()
        children = self.handle_all(node.children)
        
        b = BinarySend(
                   receiver=receiver,
                   selector=children[0].get_string(),
                   argument=children[1],
            source_position=node.getsourcepos())
        
        # for the "only same selector binary cascade" rule, see check_binary_sends    
        self.receiver = b
        return b
        
    def check_binary_sends(self, nodes):
        """
        Allow for the Self rule, that 
            "1 + 2 + 3" is valid,
        but
            "1 + 2 - 3" is not.        
        """
        seen_binary = False
        last_selector = None
        for node in nodes:
            if isinstance(node, BinarySend):
                if seen_binary and (node.selector != last_selector):
                    reason = "You can only cascade same binary operators, not '%s' and '%s'" % (last_selector, node.selector)
                    raise ParseError(node.getsourcepos(), reason)
                else:
                    seen_binary = True
                    last_selector = node.selector
            else:
                seen_binary = False
        return nodes
        
        
    
    def visit_binary_operand(self, node):
        """ essentially, this is a stripped expression """
        return self.visit_expression(node)
        
    def visit_keyword_send(self, node):
        pos = self.pos(node)
        receiver=self.receiver
        children = self.handle_all(node.children)
        keyword_position = True
        keywords = [children[i].get_string() for i in xrange(0, len(children), 2)]
        arguments = [children[i] for i in xrange(1, len(children), 2)]
        k = KeywordSend(
                    receiver=receiver,
         selector_components=keywords,
                   arguments=arguments,
             source_position=pos)
        self.receiver = k
        return k
        
    def visit_argument_form(self, node):
        """ essentially, this is a stripped expression """
        return self.visit_expression(node)
        
    
    # Object definition 
    
    def visit_slot_object(self, node):
        
        def annotation_and_slots(slots):
            if len(slots) > 0 and isinstance(slots[0], StringConstant):
                return slots[0].get_string(), slots[1:len(slots)]
            else:
                return None, slots
            
        pos = self.pos(node)
        children = self.handle_all(node.children)
        
        slots = []
        annotation = None
        code = []
        if self.get_slot() is not None:
            arguments = self.get_slot().arguments
            argument_count = self.get_slot().argument_count
        else:
            argument_count = 0
            arguments = []
        
        
        if len(children) == 2:
            annotation, slots = annotation_and_slots(children[0].children)
            code = children[1].children
        elif len(children) == 1:
            if children[0].symbol == 'slots':
                annotation, slots = annotation_and_slots(children[0].children)
            else:
                code=children[0].children
        
        parsed_arguments = [slot for slot in slots if isinstance(slot, ArgumentSlot)]
        non_arguments = [slot for slot in slots if not isinstance(slot, ArgumentSlot)]
        arguments += parsed_arguments
        assert len(arguments) == argument_count
        
        if self.get_slot() is not None:
            self.get_slot().arguments = arguments
        slots = arguments + non_arguments
        
        return Object(source_position=pos,
                           annotation=annotation,
                                slots=slots,
                                 code=code)
        
    def visit_slots(self, node):
        pos = self.pos(node)
        children = self.handle_all(node.children)
        children = self.flatten_canister(children)
        # account for empty slot list ||
        if len(children) == 1 and isinstance(children[0], StringConstant):
            if children[0].get_string() == "||":
                children=[]
        return Canister(children=children, 
                          symbol=node.symbol,
                 source_position=pos)
    
    def visit_slot_list_annotated(self, node):
        pos = self.pos(node)
        #"only the first"
        annotation = self.dispatch(node.children[0])
        with AnnotationContext(self, annotation.get_string()):
            children = self.handle_all(node.children[1:len(node.children)])
        return Canister(children=children, 
                          symbol=node.symbol,
                 source_position=pos)
    
    def visit_argument_slot(self, node):
        name = self.dispatch(node.children[0]).get_string()
        return ArgumentSlot(name=name)
    
    def visit_assignment_slot(self, node):
        # stateful. children may change current data slot.
        with SlotContext(self):
            children = self.handle_all(node.children)
            if len(children) == 2:
                # we got an initalizer
                self.get_slot().initializer = children[1]
            self.get_slot().assignable = True
            return self.get_slot()
    
    def visit_read_slot(self, node):
        # stateful. children may change current data slot
        with SlotContext(self):
            children = self.handle_all(node.children)
            assert len(children) >= 2
            self.get_slot().initializer = children[1]
            self.get_slot().assignable = False
            return self.get_slot()
    
    def visit_unary_slot_name(self, node):
        if len(node.children) == 2 and node.children[1].symbol == "STAR":
            self.get_slot().is_parent = True
        self.get_slot().source_position=node.getsourcepos()
        self.get_slot().name=node.children[0].additional_info
        return self.get_slot()
    
    def visit_binary_slot_name(self, node):
        if len(node.children) == 2:
            self.get_slot().arguments = [ArgumentSlot(name=node.children[1].additional_info)]
        self.get_slot().argument_count = 1
        self.get_slot().source_position=node.getsourcepos()
        self.get_slot().name=node.children[0].additional_info
        return self.get_slot()
    
    
    # comment
    
    def visit_comment(self, node):
        if len(node.children) == 1:
            return Comment(comment=node.children[0].additional_info, 
                       source_position=node.children[0].getsourcepos())
        else:
            c = [Comment(comment=c.additional_info, source_position=c.getsourcepos()) for c in node.children]
            return Canister(children=c, symbol=node.symbol, source_position=node.getsourcepos())
    
    # top level production
    def visit_self_source(self, node):
        return SelfSource(self.handle_all(node.children))
    
    # general visiting
    def general_nonterminal_visit(self, node):
        # print "g_n_v:\t", type(node), node
        new_node = FooNode(children=self.handle_all(node.children),
                                 symbol=node.symbol,
                        source_position=node.getsourcepos())
        # TODO
        return new_node
        
    def general_symbol_visit(self, node):
        #print "g_s_v:\t", type(node), node
        return self.visit_STRING(node)
        
    def general_visit(self, node):
        assert False
        return node
    

def parse_file(filename):
    return Parser(filename, is_file=True).parse()

############################################################################

# generated code between this line and its other occurence
class SelfToAST(object):
    def visit_self_source(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            expr = self.visit___self_source_rest_0_0(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol0(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___self_source_rest_0_0(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol1(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_comment(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol0(self, node):
        #auto-generated code, don't edit
        children = []
        expr = self.visit_statements(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol2(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_comment(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol3(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_comment(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__plus_symbol0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__plus_symbol0(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__star_symbol4(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            expr = self.visit__plus_symbol0(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            expr = self.visit_statements(node.children[1])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__plus_symbol0(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit_statements(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit__star_symbol4(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__star_symbol5(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__star_symbol5(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_statements(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            expr = self.visit___statements_rest_0_0(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol2(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___statements_rest_0_0(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_expression(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if node.children[0].symbol == 'messages':
            children = []
            expr = self.visit_messages(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_primary(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit_primary(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            if node.children[0].symbol == 'FLOAT':
                return [node.children[0]]
            if node.children[0].symbol == 'INTEGER':
                return [node.children[0]]
            if node.children[0].symbol == 'SELF':
                return [node.children[0]]
            if node.children[0].symbol == 'STRING':
                return [node.children[0]]
            return self.visit_object(node.children[0])
        return self.visit_expression(node.children[1])
    def visit_messages(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if node.children[0].symbol == 'all_sends':
            children = []
            expr = self.visit_all_sends(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        if node.children[0].symbol == 'binary_keyword_sends':
            children = []
            expr = self.visit_binary_keyword_sends(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit_keyword_sends(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol6(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_explicit_receiver(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__plus_symbol1(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend(self.visit_unary_send(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_unary_send(node.children[0]))
        expr = self.visit__plus_symbol1(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol7(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_comment(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__star_symbol8(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend(self.visit_binary_send(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_binary_send(node.children[0]))
        expr = self.visit__star_symbol8(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol9(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_comment(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol10(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_keyword_send(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit_all_sends(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            expr = self.visit___all_sends_rest_0_0(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol6(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___all_sends_rest_0_0(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol11(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_explicit_receiver(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__plus_symbol2(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend(self.visit_unary_send(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_unary_send(node.children[0]))
        expr = self.visit__plus_symbol2(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol12(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_comment(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__star_symbol13(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend(self.visit_binary_send(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_binary_send(node.children[0]))
        expr = self.visit__star_symbol13(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_unary_binary_sends(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            expr = self.visit___unary_binary_sends_rest_0_0(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol11(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___unary_binary_sends_rest_0_0(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol14(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_explicit_receiver(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__plus_symbol3(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend(self.visit_unary_send(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_unary_send(node.children[0]))
        expr = self.visit__plus_symbol3(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol15(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_comment(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit_unary_sends(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            expr = self.visit___unary_sends_rest_0_0(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol14(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___unary_sends_rest_0_0(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol16(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_explicit_receiver(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__plus_symbol4(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend(self.visit_binary_send(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_binary_send(node.children[0]))
        expr = self.visit__plus_symbol4(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol17(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_comment(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol18(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_keyword_send(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit_binary_keyword_sends(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            expr = self.visit___binary_keyword_sends_rest_0_0(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol16(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___binary_keyword_sends_rest_0_0(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol19(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_explicit_receiver(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__plus_symbol5(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend(self.visit_binary_send(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_binary_send(node.children[0]))
        expr = self.visit__plus_symbol5(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_binary_sends(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            expr = self.visit__plus_symbol5(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol19(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit__plus_symbol5(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol20(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_explicit_receiver(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit_keyword_sends(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend(self.visit_keyword_send(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol20(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        children.extend(self.visit_keyword_send(node.children[1]))
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol21(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_comment(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit_unary_send(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend([node.children[0]])
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend([node.children[0]])
        expr = self.visit__maybe_symbol21(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol22(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_comment(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol23(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_comment(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit_binary_send(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            children.extend(self.visit_operator(node.children[0]))
            expr = self.visit___binary_send_rest_0_0(node.children[1])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_operator(node.children[0]))
        expr = self.visit__maybe_symbol22(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___binary_send_rest_0_0(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_binary_operand(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if node.children[0].symbol == 'keyword_send':
            children = []
            children.extend(self.visit_keyword_send(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        if node.children[0].symbol == 'primary':
            return self.visit_primary(node.children[0])
        children = []
        expr = self.visit_unary_sends(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__star_symbol24(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            children.extend([node.children[0]])
            children.extend(self.visit_expression(node.children[1]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend([node.children[0]])
        children.extend(self.visit_expression(node.children[1]))
        expr = self.visit__star_symbol24(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_keyword_send(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            children.extend([node.children[0]])
            children.extend(self.visit_expression(node.children[1]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend([node.children[0]])
        children.extend(self.visit_expression(node.children[1]))
        expr = self.visit__star_symbol24(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_explicit_receiver(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if node.children[0].symbol == 'primary':
            return self.visit_primary(node.children[0])
        return self.visit_resend(node.children[0])
    def visit_resend(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_delegate(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit_delegate(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if node.children[0].symbol == 'NAME':
            return [node.children[0]]
        return [node.children[0]]
    def visit_object(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if node.children[0].symbol == 'block_object':
            return self.visit_block_object(node.children[0])
        return self.visit_slot_object(node.children[0])
    def visit__maybe_symbol25(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_slots(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol26(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_body(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit_slot_object(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            expr = self.visit___slot_object_rest_0_0(node.children[1])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol25(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___slot_object_rest_0_0(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol27(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_slots(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol28(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_body(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit_block_object(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            expr = self.visit___block_object_rest_0_0(node.children[1])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol27(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___block_object_rest_0_0(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol29(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_annotation(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol30(self, node):
        #auto-generated code, don't edit
        children = []
        expr = self.visit_slot_list(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_slots(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend([node.children[0]])
            return [Nonterminal(node.symbol, children)]
        if length == 2:
            children = []
            expr = self.visit___slots_rest_0_0(node.children[1])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol29(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___slots_rest_0_0(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_slot_list(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if node.children[0].symbol == 'slot_list_annotated':
            children = []
            expr = self.visit_slot_list_annotated(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit_slot_list_plain(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_annotation(self, node):
        #auto-generated code, don't edit
        return [node.children[3]]
    def visit__maybe_symbol31(self, node):
        #auto-generated code, don't edit
        children = []
        expr = self.visit_slot_list(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_slot_list_annotated(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 3:
            children = []
            children.extend([node.children[1]])
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend([node.children[1]])
        expr = self.visit__maybe_symbol31(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__star_symbol32(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            expr = self.visit_slot_list(node.children[1])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit_slot_list(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit__star_symbol32(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol33(self, node):
        #auto-generated code, don't edit
        children = []
        return [Nonterminal(node.symbol, children)]
    def visit_slot_list_plain(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            children.extend(self.visit_slot_descriptor(node.children[0]))
            expr = self.visit___slot_list_plain_rest_0_0(node.children[1])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_slot_descriptor(node.children[0]))
        expr = self.visit__star_symbol32(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___slot_list_plain_rest_0_0(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_slot_descriptor(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if node.children[0].symbol == 'argument_slot':
            return self.visit_argument_slot(node.children[0])
        if node.children[0].symbol == 'assignment_slot':
            return self.visit_assignment_slot(node.children[0])
        return self.visit_read_slot(node.children[0])
    def visit_argument_slot(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend([node.children[0]])
        return [Nonterminal(node.symbol, children)]
    def visit_read_slot(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_slot_name(node.children[0]))
        children.extend(self.visit_expression(node.children[2]))
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol34(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_expression(node.children[1]))
        return [Nonterminal(node.symbol, children)]
    def visit_assignment_slot(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend(self.visit_unary_slot_name(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_unary_slot_name(node.children[0]))
        expr = self.visit__maybe_symbol34(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_slot_name(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if node.children[0].symbol == 'binary_slot_name':
            return self.visit_binary_slot_name(node.children[0])
        if node.children[0].symbol == 'keyword_slot_name':
            return self.visit_keyword_slot_name(node.children[0])
        return self.visit_unary_slot_name(node.children[0])
    def visit__maybe_symbol35(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend([node.children[0]])
        return [Nonterminal(node.symbol, children)]
    def visit_unary_slot_name(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend([node.children[0]])
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend([node.children[0]])
        expr = self.visit__maybe_symbol35(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol36(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend([node.children[0]])
        return [Nonterminal(node.symbol, children)]
    def visit_binary_slot_name(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend(self.visit_operator(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_operator(node.children[0]))
        expr = self.visit__maybe_symbol36(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_keyword_slot_name(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if node.children[0].symbol == 'keyword_slot_name_args':
            children = []
            children.extend(self.visit_keyword_slot_name_args(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_keyword_slot_name_no_args(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit__star_symbol37(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend([node.children[0]])
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend([node.children[0]])
        expr = self.visit__star_symbol37(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_keyword_slot_name_no_args(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend([node.children[0]])
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend([node.children[0]])
        expr = self.visit__star_symbol37(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__star_symbol38(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            children.extend([node.children[0]])
            children.extend([node.children[1]])
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend([node.children[0]])
        children.extend([node.children[1]])
        expr = self.visit__star_symbol38(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_keyword_slot_name_args(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            children.extend([node.children[0]])
            children.extend([node.children[1]])
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend([node.children[0]])
        children.extend([node.children[1]])
        expr = self.visit__star_symbol38(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__star_symbol39(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            children.extend(self.visit_expression(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_expression(node.children[0]))
        expr = self.visit__star_symbol39(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_body(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend(self.visit_return(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__star_symbol39(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        children.extend(self.visit_return(node.children[1]))
        return [Nonterminal(node.symbol, children)]
    def visit_return(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if node.children[0].symbol == 'explicit_return':
            children = []
            children.extend(self.visit_explicit_return(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_expression(node.children[0]))
        return [Nonterminal(node.symbol, children)]
    def visit_explicit_return(self, node):
        #auto-generated code, don't edit
        children = []
        children.extend(self.visit_expression(node.children[1]))
        return [Nonterminal(node.symbol, children)]
    def visit__star_symbol40(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__star_symbol40(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit__maybe_symbol41(self, node):
        #auto-generated code, don't edit
        children = []
        expr = self.visit_comment(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_comment(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            children.extend([node.children[0]])
            expr = self.visit___comment_rest_0_0(node.children[1])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend([node.children[0]])
        expr = self.visit__star_symbol40(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___comment_rest_0_0(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit_operator(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if node.children[0].symbol == 'ARROW':
            return [node.children[0]]
        if node.children[0].symbol == 'BAR':
            return [node.children[0]]
        if node.children[0].symbol == 'CARET':
            return [node.children[0]]
        if node.children[0].symbol == 'EQUALS':
            return [node.children[0]]
        if node.children[0].symbol == 'OPERATOR':
            return [node.children[0]]
        return [node.children[0]]
    def visit___self_source_rest_0_0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol1(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___statements_rest_0_0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            children.extend(self.visit_expression(node.children[0]))
            expr = self.visit___statements_rest_0_1(node.children[1])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_expression(node.children[0]))
        expr = self.visit__maybe_symbol3(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___statements_rest_0_1(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___statements_rest_0_1(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            expr = self.visit___statements_rest_0_2(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__star_symbol4(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___statements_rest_0_2(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___statements_rest_0_2(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 0:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__star_symbol5(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___all_sends_rest_0_0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            expr = self.visit__plus_symbol1(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            expr = self.visit___all_sends_rest_0_1(node.children[1])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__plus_symbol1(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit__maybe_symbol7(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___all_sends_rest_0_1(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___all_sends_rest_0_1(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            expr = self.visit___all_sends_rest_0_2(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__star_symbol8(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___all_sends_rest_0_2(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___all_sends_rest_0_2(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            expr = self.visit___all_sends_rest_0_3(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol9(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___all_sends_rest_0_3(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___all_sends_rest_0_3(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 0:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol10(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___unary_binary_sends_rest_0_0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            expr = self.visit__plus_symbol2(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            expr = self.visit___unary_binary_sends_rest_0_1(node.children[1])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__plus_symbol2(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit__maybe_symbol12(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___unary_binary_sends_rest_0_1(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___unary_binary_sends_rest_0_1(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 0:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__star_symbol13(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___unary_sends_rest_0_0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            expr = self.visit__plus_symbol3(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__plus_symbol3(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit__maybe_symbol15(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___binary_keyword_sends_rest_0_0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 2:
            children = []
            expr = self.visit__plus_symbol4(node.children[0])
            assert len(expr) == 1
            children.extend(expr[0].children)
            expr = self.visit___binary_keyword_sends_rest_0_1(node.children[1])
            assert len(expr) == 1
            children.extend(expr[0].children)
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__plus_symbol4(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit__maybe_symbol17(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        expr = self.visit___binary_keyword_sends_rest_0_1(node.children[2])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___binary_keyword_sends_rest_0_1(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 0:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol18(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___binary_send_rest_0_0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            children.extend(self.visit_binary_operand(node.children[0]))
            return [Nonterminal(node.symbol, children)]
        children = []
        children.extend(self.visit_binary_operand(node.children[0]))
        expr = self.visit__maybe_symbol23(node.children[1])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___slot_object_rest_0_0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol26(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___block_object_rest_0_0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol28(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___slots_rest_0_0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 1:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol30(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___slot_list_plain_rest_0_0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 0:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol33(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def visit___comment_rest_0_0(self, node):
        #auto-generated code, don't edit
        length = len(node.children)
        if length == 0:
            children = []
            return [Nonterminal(node.symbol, children)]
        children = []
        expr = self.visit__maybe_symbol41(node.children[0])
        assert len(expr) == 1
        children.extend(expr[0].children)
        return [Nonterminal(node.symbol, children)]
    def transform(self, tree):
        #auto-generated code, don't edit
        assert isinstance(tree, Nonterminal)
        assert tree.symbol == 'self_source'
        r = self.visit_self_source(tree)
        assert len(r) == 1
        if not we_are_translated():
            try:
                if py.test.config.option.view:
                    r[0].view()
            except AttributeError:
                pass
        return r[0]
parser = PackratParser([Rule('self_source', [['_maybe_symbol0', '__self_source_rest_0_0'], ['__self_source_rest_0_0']]),
  Rule('_maybe_symbol1', [['comment']]),
  Rule('_maybe_symbol0', [['statements']]),
  Rule('_maybe_symbol2', [['comment']]),
  Rule('_maybe_symbol3', [['comment']]),
  Rule('_plus_symbol0', [['NEWLINE', '_plus_symbol0'], ['NEWLINE']]),
  Rule('_star_symbol4', [['_plus_symbol0', 'statements', '_star_symbol4'], ['_plus_symbol0', 'statements']]),
  Rule('_star_symbol5', [['NEWLINE', '_star_symbol5'], ['NEWLINE']]),
  Rule('statements', [['_maybe_symbol2', '__statements_rest_0_0'], ['__statements_rest_0_0']]),
  Rule('expression', [['messages'], ['primary']]),
  Rule('primary', [['SELF'], ['INTEGER'], ['FLOAT'], ['STRING'], ['object'], ['LEFT_PARENTHESIS', 'expression', 'RIGHT_PARENTHESIS']]),
  Rule('messages', [['all_sends'], ['binary_keyword_sends'], ['keyword_sends']]),
  Rule('_maybe_symbol6', [['explicit_receiver']]),
  Rule('_plus_symbol1', [['unary_send', '_plus_symbol1'], ['unary_send']]),
  Rule('_maybe_symbol7', [['comment']]),
  Rule('_star_symbol8', [['binary_send', '_star_symbol8'], ['binary_send']]),
  Rule('_maybe_symbol9', [['comment']]),
  Rule('_maybe_symbol10', [['keyword_send']]),
  Rule('all_sends', [['_maybe_symbol6', '__all_sends_rest_0_0'], ['__all_sends_rest_0_0']]),
  Rule('_maybe_symbol11', [['explicit_receiver']]),
  Rule('_plus_symbol2', [['unary_send', '_plus_symbol2'], ['unary_send']]),
  Rule('_maybe_symbol12', [['comment']]),
  Rule('_star_symbol13', [['binary_send', '_star_symbol13'], ['binary_send']]),
  Rule('unary_binary_sends', [['_maybe_symbol11', '__unary_binary_sends_rest_0_0'], ['__unary_binary_sends_rest_0_0']]),
  Rule('_maybe_symbol14', [['explicit_receiver']]),
  Rule('_plus_symbol3', [['unary_send', '_plus_symbol3'], ['unary_send']]),
  Rule('_maybe_symbol15', [['comment']]),
  Rule('unary_sends', [['_maybe_symbol14', '__unary_sends_rest_0_0'], ['__unary_sends_rest_0_0']]),
  Rule('_maybe_symbol16', [['explicit_receiver']]),
  Rule('_plus_symbol4', [['binary_send', '_plus_symbol4'], ['binary_send']]),
  Rule('_maybe_symbol17', [['comment']]),
  Rule('_maybe_symbol18', [['keyword_send']]),
  Rule('binary_keyword_sends', [['_maybe_symbol16', '__binary_keyword_sends_rest_0_0'], ['__binary_keyword_sends_rest_0_0']]),
  Rule('_maybe_symbol19', [['explicit_receiver']]),
  Rule('_plus_symbol5', [['binary_send', '_plus_symbol5'], ['binary_send']]),
  Rule('binary_sends', [['_maybe_symbol19', '_plus_symbol5'], ['_plus_symbol5']]),
  Rule('_maybe_symbol20', [['explicit_receiver']]),
  Rule('keyword_sends', [['_maybe_symbol20', 'keyword_send'], ['keyword_send']]),
  Rule('_maybe_symbol21', [['comment']]),
  Rule('unary_send', [['NAME', '_maybe_symbol21'], ['NAME']]),
  Rule('_maybe_symbol22', [['comment']]),
  Rule('_maybe_symbol23', [['comment']]),
  Rule('binary_send', [['operator', '_maybe_symbol22', '__binary_send_rest_0_0'], ['operator', '__binary_send_rest_0_0']]),
  Rule('binary_operand', [['unary_sends'], ['keyword_send'], ['primary']]),
  Rule('_star_symbol24', [['CAP_KEYWORD', 'expression', '_star_symbol24'], ['CAP_KEYWORD', 'expression']]),
  Rule('keyword_send', [['KEYWORD', 'expression', '_star_symbol24'], ['KEYWORD', 'expression']]),
  Rule('explicit_receiver', [['resend'], ['primary']]),
  Rule('resend', [['delegate', 'DOT']]),
  Rule('delegate', [['RESEND'], ['NAME']]),
  Rule('object', [['slot_object'], ['block_object']]),
  Rule('_maybe_symbol25', [['slots']]),
  Rule('_maybe_symbol26', [['body']]),
  Rule('slot_object', [['LEFT_PARENTHESIS', '_maybe_symbol25', '__slot_object_rest_0_0'], ['LEFT_PARENTHESIS', '__slot_object_rest_0_0']]),
  Rule('_maybe_symbol27', [['slots']]),
  Rule('_maybe_symbol28', [['body']]),
  Rule('block_object', [['LEFT_BRACKET', '_maybe_symbol27', '__block_object_rest_0_0'], ['LEFT_BRACKET', '__block_object_rest_0_0']]),
  Rule('_maybe_symbol29', [['annotation']]),
  Rule('_maybe_symbol30', [['slot_list']]),
  Rule('slots', [['BAR', '_maybe_symbol29', '__slots_rest_0_0'], ['BAR', '__slots_rest_0_0'], ['OPERATOR']]),
  Rule('slot_list', [['slot_list_annotated'], ['slot_list_plain']]),
  Rule('annotation', [['LEFT_BRACE', 'RIGHT_BRACE', 'EQUALS', 'STRING']]),
  Rule('_maybe_symbol31', [['slot_list']]),
  Rule('slot_list_annotated', [['LEFT_BRACE', 'STRING', '_maybe_symbol31', 'RIGHT_BRACE'], ['LEFT_BRACE', 'STRING', 'RIGHT_BRACE']]),
  Rule('_star_symbol32', [['DOT', 'slot_list', '_star_symbol32'], ['DOT', 'slot_list']]),
  Rule('_maybe_symbol33', [['DOT']]),
  Rule('slot_list_plain', [['slot_descriptor', '_star_symbol32', '__slot_list_plain_rest_0_0'], ['slot_descriptor', '__slot_list_plain_rest_0_0']]),
  Rule('slot_descriptor', [['argument_slot'], ['read_slot'], ['assignment_slot']]),
  Rule('argument_slot', [['ARGUMENT']]),
  Rule('read_slot', [['slot_name', 'EQUALS', 'expression']]),
  Rule('_maybe_symbol34', [['ARROW', 'expression']]),
  Rule('assignment_slot', [['unary_slot_name', '_maybe_symbol34'], ['unary_slot_name']]),
  Rule('slot_name', [['unary_slot_name'], ['binary_slot_name'], ['keyword_slot_name']]),
  Rule('_maybe_symbol35', [['STAR']]),
  Rule('unary_slot_name', [['NAME', '_maybe_symbol35'], ['NAME']]),
  Rule('_maybe_symbol36', [['NAME']]),
  Rule('binary_slot_name', [['operator', '_maybe_symbol36'], ['operator']]),
  Rule('keyword_slot_name', [['keyword_slot_name_no_args'], ['keyword_slot_name_args']]),
  Rule('_star_symbol37', [['CAP_KEYWORD', '_star_symbol37'], ['CAP_KEYWORD']]),
  Rule('keyword_slot_name_no_args', [['KEYWORD', '_star_symbol37'], ['KEYWORD']]),
  Rule('_star_symbol38', [['CAP_KEYWORD', 'NAME', '_star_symbol38'], ['CAP_KEYWORD', 'NAME']]),
  Rule('keyword_slot_name_args', [['KEYWORD', 'NAME', '_star_symbol38'], ['KEYWORD', 'NAME']]),
  Rule('_star_symbol39', [['expression', 'DOT', '_star_symbol39'], ['expression', 'DOT']]),
  Rule('body', [['_star_symbol39', 'return'], ['return']]),
  Rule('return', [['explicit_return'], ['expression']]),
  Rule('explicit_return', [['CARET', 'expression']]),
  Rule('_star_symbol40', [['NEWLINE', '_star_symbol40'], ['NEWLINE']]),
  Rule('_maybe_symbol41', [['comment']]),
  Rule('comment', [['COMMENT', '_star_symbol40', '__comment_rest_0_0'], ['COMMENT', '__comment_rest_0_0']]),
  Rule('operator', [['OPERATOR'], ['STAR'], ['BAR'], ['CARET'], ['ARROW'], ['EQUALS']]),
  Rule('__self_source_rest_0_0', [['_maybe_symbol1', 'EOF'], ['EOF']]),
  Rule('__statements_rest_0_0', [['expression', '_maybe_symbol3', '__statements_rest_0_1'], ['expression', '__statements_rest_0_1']]),
  Rule('__statements_rest_0_1', [['_star_symbol4', '__statements_rest_0_2'], ['__statements_rest_0_2']]),
  Rule('__statements_rest_0_2', [['_star_symbol5'], []]),
  Rule('__all_sends_rest_0_0', [['_plus_symbol1', '_maybe_symbol7', '__all_sends_rest_0_1'], ['_plus_symbol1', '__all_sends_rest_0_1']]),
  Rule('__all_sends_rest_0_1', [['_star_symbol8', '__all_sends_rest_0_2'], ['__all_sends_rest_0_2']]),
  Rule('__all_sends_rest_0_2', [['_maybe_symbol9', '__all_sends_rest_0_3'], ['__all_sends_rest_0_3']]),
  Rule('__all_sends_rest_0_3', [['_maybe_symbol10'], []]),
  Rule('__unary_binary_sends_rest_0_0', [['_plus_symbol2', '_maybe_symbol12', '__unary_binary_sends_rest_0_1'], ['_plus_symbol2', '__unary_binary_sends_rest_0_1']]),
  Rule('__unary_binary_sends_rest_0_1', [['_star_symbol13'], []]),
  Rule('__unary_sends_rest_0_0', [['_plus_symbol3', '_maybe_symbol15'], ['_plus_symbol3']]),
  Rule('__binary_keyword_sends_rest_0_0', [['_plus_symbol4', '_maybe_symbol17', '__binary_keyword_sends_rest_0_1'], ['_plus_symbol4', '__binary_keyword_sends_rest_0_1']]),
  Rule('__binary_keyword_sends_rest_0_1', [['_maybe_symbol18'], []]),
  Rule('__binary_send_rest_0_0', [['binary_operand', '_maybe_symbol23'], ['binary_operand']]),
  Rule('__slot_object_rest_0_0', [['_maybe_symbol26', 'RIGHT_PARENTHESIS'], ['RIGHT_PARENTHESIS']]),
  Rule('__block_object_rest_0_0', [['_maybe_symbol28', 'RIGHT_BRACKET'], ['RIGHT_BRACKET']]),
  Rule('__slots_rest_0_0', [['_maybe_symbol30', 'BAR'], ['BAR']]),
  Rule('__slot_list_plain_rest_0_0', [['_maybe_symbol33'], []]),
  Rule('__comment_rest_0_0', [['_maybe_symbol41'], []])],
 'self_source')
lexer = Lexer()
# generated code between this line and its other occurence

def make_self_parser_dynamic():
    from pypy.rlib.parsing.ebnfparse import parse_ebnf
        
    with open(os.path.join(os.path.dirname(__file__), "self.ebnf")) as f:
        filename = f.name
        ebnf_source = f.read()
        
    try:
        rs, rules, tr = parse_ebnf(ebnf_source)        
    except PyParseError,e:
        print e.nice_error_message(filename=filename,source=ebnf_source)
        raise
        
    lx = Lexer()
    pr = PackratParser(rules, rules[0].nonterminal, check_for_left_recursion=True)
    return pr, lx, tr

def make_self_parser_generated():
    return parser, lexer, SelfToAST

def make_self_parser():
    return make_self_parser_dynamic()
#    return make_self_parser_generated()

############################################################################
if __name__ == '__main__':
    f = py.path.local(__file__)
    oldcontent = f.read()
    s = "# GENERATED CODE BETWEEN THIS LINE AND ITS OTHER OCCURENCE\n".lower()
    pre, gen, after = oldcontent.split(s)

    parser, lexer, ToAST = make_self_parser_dynamic()
    transformer = ToAST.source
    newcontent = "%s%s%s\nparser = %r\nlexer = %r\n%s%s" % (
            pre, s, transformer.replace("ToAST", "SelfToAST"),
            parser, lexer, s, after)
    print newcontent
    f.write(newcontent)
