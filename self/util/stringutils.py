import string

def is_upper_digit(s):
    return is_in(s, string.ascii_uppercase + string.digits)

def is_upper(s):
    return is_in(s, string.ascii_uppercase)

def is_lower(s):
    return is_in(s, string.ascii_lowercase)

def is_digit(s):
    return is_in(s, string.digits)

def is_alnum(s):
    return is_in(s, string.ascii_letters + string.digits)

def is_alnum_under(s):
    return is_in(s, string.ascii_letters + string.digits + "_")

def is_digigt_or_minus(s):
    return is_in(s, string.digits + "-")

def is_in(s, strset):
    return s != "" and s in strset

class StringReadStream(object):
    """
    read-only stream-wrapper for string
    """
    def __init__(self, string):
        self._string = string
        self._pos = 0
        
    def close(self):
        self._string = None
        self._pos = 0
            
    def is_closed(self):
        return self._string is None
                        
    def seek(self, position, mode=0):
        if mode == 1:
            position += self._pos
        elif mode == 2:
            if position == 0:
                self._pos = len(self._string)
                return
            position += self.getsize()
        if position < 0:
            position = 0
        if position > len(self._string):
            position = len(self._string)
        self._pos = position
        
    def tell(self):
        return self._pos
        
    def read(self, n=-1):
        s = ""
        if self._pos < len(self._string) and n < 0:
            s = self._string[self._pos:]
            self._pos = len(self._string)
        else:
            if self._pos + n <= len(self._string):
                s = self._string[self._pos:self._pos+n]
                self._pos += n
            else:
                self._pos = len(self._string)
        assert isinstance(s, str)
        return s
            
    def peek(self, n=1):
        if self._pos + n > len(self._string):
            return ''
        else:
            return self._string[self._pos:self._pos+n]