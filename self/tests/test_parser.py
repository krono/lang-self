# coding=utf-8

from .base import BaseSelfTest

from self.interpreter.selfparser import Parser, ParseError
from self.interpreter.ast import *

from pypy.rlib.parsing.regex import *
from pypy.rlib.parsing.lexer import SourcePos

import pytest

class TestParser(BaseSelfTest):
    
    def test_constants(self):
        t = Parser("1").parse()
        assert t == SelfSource(expressions=[IntegerConstant(1)])
        
        t = Parser("1.1").parse()
        assert t == SelfSource(expressions=[FloatConstant(1.1)])
        
        t = Parser("''").parse()
        assert t == SelfSource(expressions=[StringConstant("")])

        t = Parser("'1'").parse()
        assert t == SelfSource(expressions=[StringConstant("1")])

        t = Parser("16rdeadbeef").parse()
        assert t == SelfSource(expressions=[IntegerConstant(0xdeadbeef)])
            
    
    def test_unary_sends(self):
        
        t = Parser("a").parse()
        assert t == SelfSource(expressions=[UnarySend(selector="a")])
        
        t = Parser("b a").parse()
        assert t == SelfSource(expressions=[UnarySend(selector="a", receiver=UnarySend(selector="b"))])

    def test_binary_sends(self):

        t = Parser("+ 1").parse()
        assert t == SelfSource(expressions=[BinarySend(selector="+", argument=IntegerConstant(1))])

        t = Parser("1 + 1").parse()
        assert t == SelfSource(expressions=[BinarySend(receiver=IntegerConstant(1), selector="+", argument=IntegerConstant(1))])

        t = Parser("a + 1").parse()
        assert t == SelfSource(expressions=[BinarySend(receiver=UnarySend(selector="a"), selector="+", argument=IntegerConstant(1))])
        
    def test_keyword_sends(self):
        t = Parser("a min: b").parse()
        assert t == SelfSource(expressions=[KeywordSend(receiver=UnarySend(selector="a"), 
                                             selector_components=['min:'],
                                                       arguments=[UnarySend(selector="b")])])
                                                       
    def test_combined_sends(self):
        t = Parser("1 a + 1").parse()
        assert t == SelfSource(expressions=[BinarySend(
                                            receiver=UnarySend(
                                                receiver=IntegerConstant(1), 
                                                selector="a"),
                                            selector="+", 
                                            argument=IntegerConstant(1))])
        t = Parser("+ pow: 3").parse()
        assert t == SelfSource(expressions=[BinarySend(
                                            selector="+", 
                                            argument=KeywordSend(
                                      selector_components=['pow:'],
                                                arguments=[IntegerConstant(3)]))])
        
        
        t = Parser("1 + 2 + 3").parse()
        assert t == SelfSource(expressions=[BinarySend(
                                            receiver=BinarySend(
                                                receiver=IntegerConstant(1),
                                                selector="+",
                                                argument=IntegerConstant(2)),
                                            selector="+", 
                                            argument=IntegerConstant(3))])

        t = Parser("1 + 2 + 3 + 4").parse()
        assert t == SelfSource(expressions=[BinarySend(selector="+",
                                            receiver=BinarySend(selector="+", 
                                                receiver=BinarySend(selector="+",
                                                    receiver=IntegerConstant(1),
                                                    argument=IntegerConstant(2)),
                                                argument=IntegerConstant(3)),
                                            argument=IntegerConstant(4))])
        
        with py.test.raises(ParseError) as e:
            t = Parser("1 + 2 - 3").parse()
        assert e.value.source_pos == SourcePos(7, 0, 7)
        assert e.value.errorinformation == "You can only cascade same binary operators, not '+' and '-'"
        
    
    def test_complex(self):
        t = Parser("bootstrap addSlotsTo: bootstrap stub -> 'globals' -> 'modules' -> () From: ( |  | )").parse()
        assert t == SelfSource(expressions=[KeywordSend(
                                  selector_components=['addSlotsTo:','From:'],
                                             receiver=UnarySend(selector="bootstrap"),
                                            arguments=[
                                                BinarySend(selector='->',
                                                    argument=Object(),
                                                    receiver=BinarySend(selector='->',
                                                        argument=StringConstant(content='modules'),
                                                        receiver=BinarySend(selector='->',
                                                            argument=StringConstant(content='globals'),                                                            
                                                            receiver=UnarySend(selector='stub',
                                                                receiver=UnarySend(selector='bootstrap'))))),
                                                Object()])])
        
    def test_simple_objects(self):
        t = Parser("()").parse()
        assert t == SelfSource(expressions=[Object()])
        
        t = Parser("(||)").parse()
        assert t == SelfSource(expressions=[Object()])
        
        t = Parser("(| |)").parse()
        assert t == SelfSource(expressions=[Object()])
    
    def test_annotated_objects(self):
        t = Parser("(| {} = 'barf' |)").parse()
        assert t == SelfSource(expressions=[Object(annotation="barf")])
        
    def test_simple_slots(self):
        t = Parser("(|x|)").parse()
        assert t == SelfSource(expressions=[Object(slots=[
            DataSlot(name="x")
        ])])
        
        t = Parser("(|x.y|)").parse()
        assert t == SelfSource(expressions=[Object(slots=[
            DataSlot(name="x"), DataSlot(name="y")
        ])])
        
        t = Parser("(|x <- 3|)").parse()
        assert t == SelfSource(expressions=[Object(slots=[
            DataSlot(name="x", initializer=IntegerConstant(content=3), assignable=True)
        ])])
    
    def test_read_only_slots(self):
        t = Parser("(|x = 3|)").parse()
        assert t == SelfSource(expressions=[Object(slots=[
            DataSlot(name="x", initializer=IntegerConstant(content=3))
        ])])
    
    #@py.test.mark.xfail
    def test_binary_slots(self):
        t = Parser("(| + a = (3) |)").parse()
        assert t == SelfSource(expressions=[Object(
                slots=[
            DataSlot(name="+", 
              initializer=Object(slots=[ArgumentSlot(name="a")],
                                  code=[IntegerConstant(content=3)]),
                arguments=[ArgumentSlot(name="a")],
           argument_count=1
            )
        ])])
        
        t = Parser("(| + = (|:a| 3) |)").parse()
        assert t == SelfSource(expressions=[Object(
                slots=[
            DataSlot(name="+", 
              initializer=Object(slots=[ArgumentSlot(name="a")],
                                  code=[IntegerConstant(content=3)]),
                arguments=[ArgumentSlot(name="a")],
           argument_count=1
            )
        ])])
        
    
    def test_annotated_slots(self):
        t = Parser("(| { 'barf' x } |)").parse()
        assert t == SelfSource(expressions=[Object(slots=[
            DataSlot(name="x", annotation="barf")
        ])])
        
        t = Parser("(| { 'barf' { 'forb' x } } |)").parse()
        assert t == SelfSource(expressions=[Object(slots=[
            DataSlot(name="x", annotation="barf\nforb")
        ])])
        
        # this is dumb, but nonetheless...
        t = Parser("(| { 'barf' } |)").parse()
        assert t == SelfSource(expressions=[Object()])
        
    
    def test_comments(self):
        t = Parser(""" "barf" """).parse()
        assert t == SelfSource(expressions=[])

        t = Parser(""" "barf" "bloep" """).parse()
        assert t == SelfSource(expressions=[])

        t = Parser(""" "barf" 
"bloep" """).parse()
        assert t == SelfSource(expressions=[])
        

    #
    #
    #
    #
    # these were simple inital file test. to succeed.
    #
    def test_file_bading(self):
        t = Parser("""a min: b""").parse()
        assert True


    def test_file_badong(self):
        t = Parser("""1 min: 2""").parse()
        assert True


    def test_file_bar(self):
        t = Parser("""" test  "
1 
1 barf 
1 + 1 
1 min: 2 
1 a: 2 P: 3 G: 4 s: 5""").parse()
        assert True


    def test_file_baz(self):
        t = Parser("""a: 2 B: 3""").parse()
        assert True


    def test_file_berf(self):
        t = Parser("""1 
1 barf 
1 + 1 
" f "
1 min: 2 
1 a: 2 P: 3 G: 4 s: 5""").parse()
        assert True


    def test_file_blerf(self):
        t = Parser("""''

'\n'

'1'""").parse()
        assert True


    def test_file_blerpo(self):
        t = Parser("""5 + 1 min: 3
+ 1 min: 3
5 + min: 3
+ min: 3
+ nextPrime min:3""").parse()
        assert True


    def test_file_blubb(self):
        t = Parser("""1""").parse()
        assert True


    def test_file_boot(self):
        t = Parser("""bootstrap addSlotsTo: bootstrap stub -> 'globals' -> 'modules' -> () From: ( |  | )""").parse()
        assert True


    def test_file_buzz(self):
        t = Parser("""'1'""").parse()
        assert True


    def test_file_del(self):
        t = Parser("""resend.barf""").parse()
        assert True


    def test_file_deldel(self):
        t = Parser("""parent.barf""").parse()
        assert True


    def test_file_empty(self):
        t = Parser("""""").parse()
        assert t == SelfSource(expressions=[])


    def test_file_fizz(self):
        t = Parser("""1 a: 2 B: 3""").parse()
        assert True


    def test_file_foo(self):
        t = Parser("""" test "
self a: b P: d G: f s: n $$  2""").parse()
        assert True


    def test_file_obj(self):
        t = Parser("""()

(||)""").parse()
        assert True


    def test_file_quirk(self):
        t = Parser("""1 nextPrime + 1 min: 1""").parse()
        assert True


    def test_file_self(self):
        t = Parser("""self""").parse()
        assert True

