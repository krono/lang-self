# coding=utf-8

from .base import BaseSelfTest

import py

from self.interpreter.lexer import Lexer, LexerError
from pypy.rlib.parsing.lexer import Token, SourcePos


def it(should=None, beLexedTo=[], shouldnt=None, beLexedAt=None):
    if should is not None:
        t = Lexer().tokenize_string(should)
        assert t == beLexedTo
    elif shouldnt is not None:
        with py.test.raises(LexerError) as e:
            Lexer().tokenize_string(shouldnt)
        assert e.value.pos == beLexedAt
    else:
        assert False

class TestLexer(BaseSelfTest):
    
    def test_simple(self):
        it(should="",   beLexedTo=[Token('EOF', '', SourcePos(0, 0, 0))])
        it(should=" ",  beLexedTo=[Token('EOF', '', SourcePos(1, 0, 1))])
        it(should="\t", beLexedTo=[Token('EOF', '', SourcePos(1, 0, 1))])
        it(should="\v", beLexedTo=[Token('EOF', '', SourcePos(1, 0, 1))])
        it(should="\b", beLexedTo=[Token('EOF', '', SourcePos(1, 0, 1))])
        it(should="\f", beLexedTo=[Token('EOF', '', SourcePos(1, 0, 1))])
        
    def test_numbers(self):
        def ok_number(tok, num, expected):
            l = len(num)
            it(should=num, beLexedTo=[
                Token(tok, str(expected), SourcePos(l, 0, l)),
                Token('EOF', '', SourcePos(l, 0, l))
            ])
        
        ok_number("INTEGER", "1",           1)
        ok_number("FLOAT",   "1.1",         1.1)
        ok_number("FLOAT",   "1e10",        1e10)
        ok_number("FLOAT",   "1.1e10",      1.1e10)
        ok_number("FLOAT",   "1.1E-9",      1.1E-9)
        ok_number("FLOAT",   "1.1e+3",      1.1e+3)
        ok_number("INTEGER", "10r10",       10)
        ok_number("INTEGER", "8R07",        7)
        ok_number("INTEGER", "16rdeadbeef", 0xdeadbeef)
        ok_number("INTEGER", "16rCAFEBABE", 0xcafebabe)
        ok_number("INTEGER", "16rAffe",     0xaffe)
        ok_number("INTEGER", "36rZOMG",     1664872)
        
        it(shouldnt="1ef",   beLexedAt=SourcePos(3, 0, 3))
        it(shouldnt="1.1ef", beLexedAt=SourcePos(5, 0, 5))
        it(shouldnt="10rA",  beLexedAt=SourcePos(4, 0, 4))
        it(shouldnt="8r8",   beLexedAt=SourcePos(3, 0, 3))
        it(shouldnt="37rb",  beLexedAt=SourcePos(3, 0, 3))
        it(shouldnt="1r0",   beLexedAt=SourcePos(2, 0, 2))
        
    def test_name(self):
        
        it(should="a", beLexedTo=[                
            Token('NAME', 'a', SourcePos(1, 0, 1)),
            Token('EOF', '', SourcePos(1, 0, 1))
        ])
        
        it(should="_a", beLexedTo=[                
            Token('PRIMITIVE_NAME', '_a', SourcePos(2, 0, 2)),
            Token('EOF', '', SourcePos(2, 0, 2))
        ])
        
        
        it(should="notAloneAtAll", beLexedTo=[                
            Token('NAME', 'notAloneAtAll', SourcePos(13, 0, 13)),
            Token('EOF', '', SourcePos(13, 0, 13))
        ])
        
        it(should="a:", beLexedTo=[                
            Token('KEYWORD', 'a:', SourcePos(2, 0, 2)),
            Token('EOF', '', SourcePos(2, 0, 2))
        ])
        
        it(should="_a:", beLexedTo=[                
            Token('PRIMITIVE_KEYWORD', '_a:', SourcePos(3, 0, 3)),
            Token('EOF', '', SourcePos(3, 0, 3))
        ])
        
        it(should="A:", beLexedTo=[                
            Token('CAP_KEYWORD', 'A:', SourcePos(2, 0, 2)),
            Token('EOF', '', SourcePos(2, 0, 2))
        ])
        
        it(should=":a", beLexedTo=[                
            Token('ARGUMENT', 'a', SourcePos(2, 0, 2)),
            Token('EOF', '', SourcePos(2, 0, 2))
        ])
        
    def test_comment(self):
        
        it(should='"1"', beLexedTo=[                
            Token('COMMENT', '1', SourcePos(3, 0, 3)),
            Token('EOF', '', SourcePos(3, 0, 3))
        ])
        
        it(should="\"\n\"", beLexedTo=[                
            Token('COMMENT', "\n", SourcePos(3, 1, 1)),
            Token('EOF', '', SourcePos(3, 1, 1))
        ])
    
    def test_escapes(self):
        it(shouldnt="'\\o7 1'", beLexedAt=SourcePos(6,0,6))
    
    def test_newline(self):
        it(should="\n", beLexedTo=[
            Token('NEWLINE', "", SourcePos(1, 1, 0)),
            Token('EOF', "", SourcePos(1, 1, 0))
        ])
    
    def test_self_suite(self):
        """
        These tests are from the Lexer in the self-witten Self-parser.
        The original messages were
            testToken:ShouldFail:Selecting:
            testToken:ShouldBe:Value:
        Hence the unusual naming.
        """
        
        token_mapping = {
            'comment': 'COMMENT',
            'operator': 'OPERATOR',
            'arrow': 'ARROW',
            'equals': 'EQUALS',
            'star': 'STAR',
            'delegatee': 'NAME',
            'primName': 'PRIMITIVE_NAME',
            'primKeyword': 'PRIMITIVE_KEYWORD',
            'name': 'NAME',
            'capKeyword': 'CAP_KEYWORD',
            'string': 'STRING',
            'float': 'FLOAT',
            'dot': 'DOT',
            'escapedNewLine': 'EOF',
            'annotationStart': 'LEFT_BRACE',
            'objectAnnotation': 'LEFT_BRACE', #hack ...
            'openParen': 'LEFT_PARENTHESIS',
            'openSquare': 'LEFT_BRACKET',
            'receiver': 'SELF',
            'resendToken': 'RESEND',
            'closeParen': 'RIGHT_PARENTHESIS',
            'annotationEnd': 'RIGHT_BRACE',
            'closeSquare': 'RIGHT_BRACKET',
            'integer': 'INTEGER',
            'bar': 'BAR',
            'colon': 'COLON',
            'argument': 'ARGUMENT',
            'hat': 'CARET',
        }
        def test_token_fails(source, should_fail, selecting):
            with py.test.raises(LexerError) as e:
                t = Lexer().tokenize_string(source)
            assert e.value.pos.i == len(selecting)
            
        def test_token_passes(source, should_be, value):
            def test_value(result, expected, orig_source):
                if isinstance(expected, str):
                    assert result == expected
            
            t = Lexer().tokenize_string(source)
            # only first token relevant
            the_token = t[0]
            assert the_token.name == token_mapping[should_be]
            if not value == None:
                test_value(the_token.source, value, source)
            
        def test_token(source, should_fail=None, selecting=None, should_be=None, value=None):
            if not (should_fail == None or selecting == None):
                test_token_fails(source, should_fail, selecting)
            elif not should_be == None:
                test_token_passes(source, should_be, value)
            else:
                assert False, "the test_token method was used inappropriately"
        
        test_token( 'self:', should_fail='using `self\' as keyword: self:', selecting='self:')
        test_token( ':A', should_fail='arguments must be uncapitalized', selecting=':A')
        test_token( ':abc:', should_fail='colon cannot both start and end token', selecting=':abc:')
        test_token( '\'\\p\'', should_fail='unknown escape sequence', selecting='\'\\p')
        test_token( '\'\\xag\'', should_fail='invalid digit in character escape', selecting='\'\\xag')
        #test_token( '\'\\o900\'', should_fail='invalid digit in character escape', selecting='\'\\o9')
        test_token( '\'\\d300\'', should_fail='invalid character escape value (>255)', selecting='\'\\d300')
        test_token( '"as', should_fail='EOF in middle of comment', selecting='"as')
        test_token( '"\\', should_fail='EOF in middle of comment', selecting='"\\')
        test_token( '\'asd', should_fail='missing trailing quote of string literal', selecting='\'asd')
        test_token( '.1', should_fail='Floats must have a digit before the decimal point', selecting='.1')
        test_token( 'Resend', should_fail='name tokens cannot be capitalized', selecting='Resend')
        test_token( '0x', should_fail='invalid numeric constant (use 16r...)', selecting='0x')
        test_token( '03', should_fail='C-style octals are not supported (use 8r...)', selecting='03')
        test_token( '6r ', should_fail='expecting a number after the base specifier', selecting='6r')
        test_token( '1r0', should_fail='base must be between 2 and 36', selecting='1r')
        test_token( '50r1', should_fail='base must be between 2 and 36', selecting='50r')
        test_token( '8ra' , should_fail='letter too large for given base', selecting='8ra')
        test_token( '\'asdf', should_fail='missing trailing quote of string literal', selecting='\'asdf')
        
        for op in ["~", "@@", "#", "$$", "%", "^^", "&", "**", "-", "+", "==", "||", "!=", "/", "?"]:
            test_token( op, should_be='operator', value=op)
        test_token( '<-', should_be='arrow')
        test_token( '=', should_be='equals', value='=')
        test_token( '*', should_be='star', value='*')
        test_token( 'foobar.+', should_be='delegatee', value='foobar')
        test_token( '_pKw', should_be='primName', value='_pKw')
        test_token( '_pKw:', should_be='primKeyword', value='_pKw:')
        test_token( 'fred23Bar', should_be='name', value='fred23Bar')

        test_token( 'Resend3:', should_be='capKeyword', value='Resend3:')

        #test_token( '\'\\x13\\o123\\d145\'', should_be='string', value='\x13\o123\d145')
        test_token( '\'\\x13\\o123\\d145\'', should_be='string', value='\x13S\x91')
        test_token( '\'a\\\n\\\r\\n\\t\\f\\r\\v\\b\\a\\0\\\'\\"\\?\\\\\'', should_be='string', value="a\n\t\f\r\v\b\a\0\'\"?\\")

        test_token( '\'asdf\'', should_be='string', value='asdf')
        test_token( '-2.3e-1', should_be='float', value=-0.23)
        test_token( '1.2', should_be='float', value=1.2)
        test_token( '1.2', should_be='float', value=1.2)
        test_token( '"comment"', should_be='comment', value='comment')
        test_token( '""', should_be='comment', value='')
        #test_token( '"a\\"\\b\\\\"', should_be='comment', value='a"b\\')
        test_token( "\"a\\\"\\b\\\\\"", should_be='comment', value='a"b\\')
        test_token( '.', should_be='dot')
        test_token( '\\\n', should_be='escapedNewLine')
        test_token( '{', should_be='annotationStart')
        test_token( '{}', should_be='objectAnnotation')
        test_token( '(', should_be='openParen')
        test_token( '[', should_be='openSquare')
        test_token( 'self', should_be='receiver')
        test_token( 'resend', should_be='resendToken')
        test_token( ')', should_be='closeParen')
        test_token( '}', should_be='annotationEnd')
        test_token( ']', should_be='closeSquare')
        test_token( '0', should_be='integer', value=0)
        test_token( '|', should_be='bar')
        #test_token( ':', should_be='colon')
        # we don't support pure colon.
        test_token( ':abc', should_be='argument', value='abc')
        test_token( '^', should_be='hat')
        test_token( '-1', should_be='integer', value=-1)
        test_token( '1. 2', should_be='integer', value=1)
        test_token( '3r12', should_be='integer', value=5)
        test_token( '-16raa', should_be='integer', value=-170)
        
        
        