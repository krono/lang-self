from contextlib import contextmanager

import py

class BaseSelfTest(object):
    @contextmanager
    def raises(self, space, exc_name, msg=None):
        # with py.test.raises(SelfError) as exc:
        #     yield
        # assert space.getclass(exc.value.w_value).name == exc_name
        if msg is not None:
            assert exc.value.w_value.msg == msg

    # def unwrap(self, space, w_obj):
    #     if isinstance(w_obj, W_FixnumObject):
    #         return space.int_w(w_obj)
    #     elif isinstance(w_obj, W_FloatObject):
    #         return space.float_w(w_obj)
    #     elif isinstance(w_obj, W_TrueObject):
    #         return True
    #     elif isinstance(w_obj, W_FalseObject):
    #         return False
    #     elif isinstance(w_obj, W_StringObject):
    #         return space.str_w(w_obj)
    #     elif isinstance(w_obj, W_SymbolObject):
    #         return space.symbol_w(w_obj)
    #     elif isinstance(w_obj, W_ArrayObject):
    #         return [self.unwrap(space, w_x) for w_x in space.listview(w_obj)]
    #     elif isinstance(w_obj, W_ModuleObject):
    #         return w_obj
    #     elif w_obj is space.w_true:
    #         return True
    #     elif w_obj is space.w_false:
    #         return False
    #     elif w_obj is space.w_nil:
    #         return None
    #     raise NotImplementedError(type(w_obj))
